package calc_figures.figures;

public interface IFigureCalculate {
	double calculateArea(double ...dimensions);
	double calculateCircuit(double ...dimensions);
}
