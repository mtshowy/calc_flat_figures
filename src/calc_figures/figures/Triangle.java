package calc_figures.figures;

public class Triangle implements IFigureCalculate {

    @Override
    public double calculateArea(double... dimensions)   {
        return (dimensions[0] * dimensions [2]) / 2;
    }

    @Override
    public double calculateCircuit(double... dimensions) {
        return dimensions[0] + dimensions [1] + dimensions [2];
    }

}
