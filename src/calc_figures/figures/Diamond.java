package calc_figures.figures;

public class Diamond implements IFigureCalculate {

    @Override
    public double calculateArea(double... dimensions)   {
        return dimensions[0] * dimensions [1];
    }

    @Override
    public double calculateCircuit(double... dimensions) {
        return 4*dimensions[0];
    }
}
