package calc_figures.circle;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import calc_figures.square.ValueGiver;

public class CirclePanelController extends ValueGiver {

    @FXML
    private TextField circleRInput;

    @FXML
    @Override
    public void transferData() {
        try {
            Double circleRI = Double.parseDouble(circleRInput.getText());
            giveValues(circleRI);
        } catch (Exception e) {
            System.out.println("You gave wrong number!");
        }
    }
}
