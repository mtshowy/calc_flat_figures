package calc_figures.triangle;

import calc_figures.square.ValueGiver;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.shape.Ellipse;

import java.awt.*;

public class TrianglePanelController extends ValueGiver {

	@FXML private TextField triangleAInput, triangleBInput, triangleCInput;

	@FXML
	@Override
	public void transferData() {
		try {
			Double triangleAI = Double.parseDouble(triangleAInput.getText());
			Double triangleBI = Double.parseDouble(triangleBInput.getText());
			Double triangleCI = Double.parseDouble(triangleCInput.getText());
			giveValues(triangleAI, triangleBI, triangleCI);
		} catch (Exception e) {
			System.out.println("You gave wrong number");
		}
	}
}
