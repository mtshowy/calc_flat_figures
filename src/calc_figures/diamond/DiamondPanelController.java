package calc_figures.diamond;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import calc_figures.square.ValueGiver;

public class DiamondPanelController extends ValueGiver {

    @FXML
    private TextField diamondAInput;
    @FXML
    private TextField diamondHInput;

    @FXML
    @Override
    public void transferData() {
        try {
            Double diamondAI = Double.parseDouble(diamondAInput.getText());
            Double diamondHI = Double.parseDouble(diamondHInput.getText());
            giveValues(diamondAI, diamondHI);
        } catch (Exception e) {
            System.out.println("You gave wrong number!");
        }
    }
}
