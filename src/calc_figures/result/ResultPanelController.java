package calc_figures.result;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class ResultPanelController {

	@FXML private TextField circuitResult, areaResult;

	//Gettery wyników pola/obwodu figury
    public TextField getAreaResult() {
        return areaResult;
    }
    public TextField getCircuitResult() {
        return circuitResult;
    }

	//Settery do TextFieldów
	public void setAreaResult(TextField areaResult) {
		this.areaResult = areaResult;
	}
	public void setCircuitResult(TextField circuitResult) {
		this.circuitResult = circuitResult;
	}

	//Settery do wyświetlenia wyniku (string) w TextField
	public void setTextToAreaResult(String value) {
		areaResult.setText(value);
	}
	public void setTextToCircuitResult(String value) {
    	circuitResult.setText(value);
	}

	//Wyczyszczenie wartości w wynikach przy zmianie figury
	public void clearResults() {
		circuitResult.clear();
		areaResult.clear();
	}
}
