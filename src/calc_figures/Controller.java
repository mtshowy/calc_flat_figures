package calc_figures;

import calc_figures.figures.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import java.io.IOException;

import calc_figures.result.ResultPanelController;
import calc_figures.square.ValueGiver;
import calc_figures.square.ValueListener;

public class Controller implements ValueListener {

	@FXML
	private BorderPane rootPane;
	private ResultPanelController resultPanelController;
	private Button squareButton, rectangleButton, diamondButton, circleButton;

	IFigureCalculate calculator;

	public void prepareSquarePanel() throws Exception {
		calculator = new Square();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("square/squarePanel.fxml"));
		setContentOfContentPane(loader);
		registerValueGiver(loader.getController());
		prepareResultPanel();
		clearResults();
	}

	public void prepareRectanglePanel() throws Exception {
		calculator = new Rectangle();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("rectangle/rectanglePanel.fxml"));
		setContentOfContentPane(loader);
		registerValueGiver(loader.getController());
		prepareResultPanel();
		clearResults();
	}

	public void prepareDiamondPanel() throws Exception {
		calculator = new Diamond();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("diamond/diamondPanel.fxml"));
		setContentOfContentPane(loader);
		registerValueGiver(loader.getController());
		prepareResultPanel();
		clearResults();
	}

	public void prepareTrianglePanel() throws Exception {
		calculator = new Triangle();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("triangle/trianglePanel.fxml"));
		setContentOfContentPane(loader);
		registerValueGiver(loader.getController());
		prepareResultPanel();
		clearResults();
	}


	public void prepareCirclePanel() throws Exception {
		calculator = new Circle();
		FXMLLoader loader = new FXMLLoader(getClass().getResource("circle/circlePanel.fxml"));
		setContentOfContentPane(loader);
		registerValueGiver(loader.getController());
		prepareResultPanel();
		clearResults();
	}


	private void prepareResultPanel() throws IOException {
		if (resultPanelController != null)
			return;

		FXMLLoader loader = new FXMLLoader(getClass().getResource("result/resultPanel.fxml"));
		rootPane.setBottom(loader.load());
		resultPanelController = loader.getController();
	}

	private void registerValueGiver(Object valueGiver) throws Exception {
		if (!(valueGiver instanceof ValueGiver))
			throw new Exception();
			((ValueGiver) valueGiver).registerListener(this);
	}

	private void setContentOfContentPane(FXMLLoader loader) throws IOException {
		Parent parent = loader.load();
		rootPane.setCenter(parent);
	}

	private void clearResults() {
		resultPanelController.clearResults();
	}

	@Override
	public void preProcessData(double... values) {
		double area = calculator.calculateArea(values);
		double circuit = calculator.calculateCircuit(values);
		setValuesToResultPanel(area, circuit);
	}

	private void setValuesToResultPanel(double area, double circuit) {
		resultPanelController.setTextToAreaResult(Double.toString(area));
		resultPanelController.setTextToCircuitResult(Double.toString(circuit));
	}

	private void buttonCSS() {
		squareButton.getStyleClass().add("button-square");
		rectangleButton.getStyleClass().add("button-rectangle");
		diamondButton.getStyleClass().add("button-diamond");
		circleButton.getStyleClass().add("button-circle");
	}

}

