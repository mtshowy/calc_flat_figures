package calc_figures.square;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class SquarePanelController extends ValueGiver {

	@FXML private TextField squareSideInput;
	@FXML private Button buttonResult;

	@Override
	public void transferData() {
		double squareSideI = 0;
		try {
			squareSideI = Double.parseDouble(squareSideInput.getText());
			giveValues(squareSideI);
		} catch (Exception e) {
			System.out.println("You gave wrong number");
		}
	}

	private void resultButtonCSS(){
		buttonResult.getStyleClass().add("button-result");
	}
}
