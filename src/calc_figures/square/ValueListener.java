package calc_figures.square;

public interface ValueListener {
	void preProcessData(double... values);
}
